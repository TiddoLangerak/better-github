import { Pull } from "./pull";
import { PullFile } from "./file";
import { getGithubToken } from "./token";

interface GraphqlError {
  message: string;
}
type QueryResponse<T> = {
  data: T,
} | {
  errors: GraphqlError[]
}

async function v4(query: string, variables?: object) {
  const body = JSON.stringify({ query, variables });
  const githubToken = await getGithubToken();
  const response = await fetch("https://api.github.com/graphql", {
    method: "POST",
    body,
    mode: 'cors',
    headers: {
      'Authorization': `Bearer ${githubToken}`
    }
  })
  return await response.json()
}

type FilesQueryResponse = QueryResponse<{
  repository: {
    pullRequest: {
      files: {
        nodes: PullFile[]
      }
    }
  }
}>;


export async function listFiles({ owner, repo, pull }: Pull): Promise<PullFile[]> {
  const response: FilesQueryResponse = await v4(`
    query($owner:String!,$name:String!,$number:Int!) {
      repository(owner:$owner,name:$name) {
        pullRequest(number:$number) {
          files(first:100) {
            nodes {
              path,
              additions,
              deletions
            }
          }
        }
      }
    }
  `, { owner, name: repo, number: pull });

  if ('errors' in response) {
    const errorMessage = response.errors
      .map((error, index) => `[${index+1}]: ${error.message}`)
      .join('\n');
    throw new Error(`GraphQL request failed:\n${errorMessage}`);
  }

  return response.data.repository.pullRequest.files.nodes.map(
    (file, index) => ({
      ...file,
      id: index
    })
  );
}
