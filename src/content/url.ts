import { Pull } from "./pull";

const pullPattern = /https:\/\/github.com\/(.*?)\/(.*?)\/pull\/(\d*)(?:\/(.*))?/;

export function getPullParts(): Pull | null {
  const match = pullPattern.exec(window.location.href);
  if (!match) {
    return null;
  }
  return {
    owner: match[1],
    repo: match[2],
    pull: Number(match[3]),
    page: match[4],
  }
}

