export interface PullFile {
  path: string;
  additions: number;
  deletions: number;
  id: number;
}

export interface PullFileTreeLeaf {
  type: 'file';
  additions: number;
  deletions: number;
  id: number;
}

export interface PullFileTreeNode {
  type: 'folder';
  children: {
    [k: string]: PullFileTreeNode | PullFileTreeLeaf;
  }
}

export type PullFileTree = PullFileTreeNode | PullFileTreeLeaf;

function fileToTree({ additions, deletions, path, id }: PullFile): PullFileTree {
  const parts = path.split('/');
  const leaf : PullFileTreeLeaf = { type:'file', additions, deletions, id };

  return parts
    .reduceRight((acc: PullFileTree, pathPart) => {
      return {
        type: 'folder',
        children: {
          [pathPart] : acc
        }
      };
    }, leaf);
}

function mergeTrees(a: PullFileTree, b: PullFileTree) : PullFileTree {
  if (a.type === 'file' || b.type === 'file') {
    throw new Error('Cannot merge files');
  }
  const intersection = Object.entries(a.children)
    .filter(([key, val]) => key in b.children)
    .map(([key, val]) => {
      return {
        [key]: mergeTrees(val, b.children[key])
      };
    })
    .reduce((a, b) => ({ ...a, ...b }), {});

  return {
    type: 'folder',
    children: {
      ...a.children,
      ...b.children,
      ...intersection
    }
  };
}

export function filesToTree(files: PullFile[]): PullFileTree {
  return files
    .map(fileToTree)
    .reduce(mergeTrees, { type: 'folder', children: {} });
}

export function flattenDirectories(tree: PullFileTree) : PullFileTree {
  if (tree.type === 'file') {
    return tree;
  }

  const children = Object.entries(tree.children);

  const flattenedKids = children
    .map<[string, PullFileTree]>(([childName, child]) => [childName, flattenDirectories(child)])
    .map<[string, PullFileTree]>(([childName, child]) => {
      if (child.type === 'folder') {
        const grandKids = Object.entries(child.children);
        if (grandKids.length === 1) {
          const [grandKidName, grandKid] = grandKids[0];
          if (grandKid.type === 'folder') {
            return [
              `${childName}/${grandKidName}`,
              grandKid
            ];
          }
        }
      }

      return [childName, child];
    })
    .reduce((acc, [name, node]) => ({
      ...acc,
      [name]: node
    }), {});

  return {
    type: 'folder',
    children: flattenedKids
  };
}
