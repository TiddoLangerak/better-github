export interface Pull {
  owner: string;
  repo: string;
  pull: number;
  page: string;
}

