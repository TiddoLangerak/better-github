export async function getGithubToken() : Promise<string | undefined> {
  const result = await browser.storage.sync.get('ghKey');
  return result.ghKey as (string | undefined);
}
