import { onMessage } from "../common/message";
import { getPullParts } from "./url";
import { listFiles } from "./api";
import { filesToTree, flattenDirectories, PullFileTreeNode } from "./file";
import { attachFileTree } from "./widgets/filetree";
import { getGithubToken } from "./token";

async function load() {
  const parts = getPullParts();
  if (!parts) {
    return;
  }

  if (!(await getGithubToken())) {
    console.warn("better-github won't work until github token is provided");
    return;
  }

  if (parts.page === 'files') {
    const files = await listFiles(parts);
    const flattenedTree = flattenDirectories(filesToTree(files));
    attachFileTree(flattenedTree as PullFileTreeNode);
  }
}

onMessage(m => {
  switch(m.type) {
    case 'HISTORY_STATE_UPDATED':
      load();
      break;
  }
});

load();

