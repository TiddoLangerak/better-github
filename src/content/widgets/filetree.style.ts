const containerWidth = "980px";
const scrollbarWidth = "12px";
const totalWhitespace = `(100vw - ${scrollbarWidth} - ${containerWidth})`;
const whitespaceWidth = `(${totalWhitespace}/2)`
const anchorLeftFormula = `-1 * ${whitespaceWidth}`;

export const rootContainerStyle = {
  position: 'relative',
};

export const rootAnchorStyle = {
  position: 'absolute',
  left: `calc((${anchorLeftFormula}))`,
  minWidth: `calc(${whitespaceWidth})`,
  backgroundColor: 'white',
  border: '2px gray solid',
  overflow: 'auto',
  maxHeight: '90vh'
}

export const folderLiStyle = {
  marginLeft: '10px',
  listStyleType: 'none'
}

export const fileAdditions = {
  color: 'green'
}

export const fileDeletions = {
  color: 'red'
}

export const fileLink = {
  color: 'inherit'
}

export const fileStyle = {
  marginLeft: '10px'
}

export const rootListStyle = {
  borderTop: '1px gray dashed' }
