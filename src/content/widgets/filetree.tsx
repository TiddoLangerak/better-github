import React from 'react';
import ReactDOM from 'react-dom';
import { PullFile, PullFileTree, PullFileTreeLeaf, PullFileTreeNode } from "../file";
import { rootContainerStyle, rootAnchorStyle, folderLiStyle, fileDeletions, fileAdditions, fileLink, fileStyle, rootListStyle } from "./filetree.style";

function getOrCreateContainer() {
  const existing = document.getElementById('better-gh-filetree');
  if (existing) {
    return existing;
  } else {
    const container = document.createElement('div');
    container.id = 'better-gh-filetree';
    return container;
  }
}

export function attachFileTree(tree: PullFileTreeNode) {
  const container = getOrCreateContainer();
  ReactDOM.unmountComponentAtNode(container);
  ReactDOM.render(<Root tree={tree} />, container);
  document.getElementsByClassName('pr-toolbar')[0]
    .appendChild(container);

}

interface RootProps {
  tree: PullFileTreeNode;
};

function Root({ tree } : RootProps) {
  console.log(rootContainerStyle, rootAnchorStyle);
  return <div className="better-gh-filetree-container" style={rootContainerStyle}>
    <div className="better-gh-filetree-anchor" style={rootAnchorStyle}>
      <details>
        <summary>Changes</summary>
        <ul style={rootListStyle}>
          { Object.entries(tree.children).map(([name, val]) => <SubTree name={name} tree={val} key={name} />) }
        </ul>
      </details>
    </div>
  </div>;
}

interface SubTreeProps {
  tree: PullFileTree;
  name: string;
};

function SubTree({ tree, name }: SubTreeProps) {
  if (tree.type === 'folder') {
    return <Folder folder={tree} name={name} />
  } else {
    return <File file={tree} name={name}/>
  }
}

interface FolderProps {
  folder: PullFileTreeNode;
  name: string;
};

function Folder({ folder, name }: FolderProps) {
  const kids = Object.entries(folder.children)
    .map(([name, val]) => {
      return (
        <li key={name} style={folderLiStyle}>
          <SubTree tree={val} name={name} />
        </li>
      );
    });
  return (
    <details open>
      <summary>📁 { name }</summary>
      <ul>
        {kids}
      </ul>
    </details>
  )
}

interface FileProps {
  file: PullFileTreeLeaf;
  name: string;
}

function File({ file, name }: FileProps) {
  return (
    <span style={fileStyle}>
      <a href={`#diff-${file.id}`} style={fileLink}>📄 {name}</a>
      <span style={fileAdditions}>+{file.additions}</span> <span style={fileDeletions}>-{file.deletions}</span>
    </span>
  )
}
