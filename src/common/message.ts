export type Message = {
  type: 'HISTORY_STATE_UPDATED'
}

export function sendToTab(tabId: number, m: Message) {
  browser.tabs.sendMessage(tabId, m);
}

export function onMessage(listener: (m: Message) => unknown) {
  browser.runtime.onMessage.addListener(listener as any);
}
