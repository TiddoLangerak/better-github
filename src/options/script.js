function saveOptions(e) {
  console.log("Saving option");
  e.preventDefault();
  browser.storage.sync.set({
    ghKey: document.querySelector("#gh-key").value
  });
}

function restoreOptions() {

  function setCurrentChoice(result) {
    document.querySelector("#gh-key").value = result.ghKey || "";
  }

  function onError(error) {
    console.log(`Error: ${error}`);
  }

  var getting = browser.storage.sync.get("ghKey");
  getting.then(setCurrentChoice, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
