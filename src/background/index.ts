import { sendToTab } from "../common/message";

browser.webNavigation.onHistoryStateUpdated.addListener(event => {
  sendToTab(event.tabId, { type: 'HISTORY_STATE_UPDATED' });
}, {
  url: [{ hostEquals: 'github.com' }]
});

