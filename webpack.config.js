const path = require('path');

module.exports = (env, argv) => ({
  entry: {
    content: './src/content/index.ts',
    background: './src/background/index.ts'
  },
  devtool: argv.mode === 'production' ? undefined : 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx']
  },
  output: {
    filename: '[name]/better-gh.js',
    path: path.resolve(__dirname, 'dist')
  }
});
